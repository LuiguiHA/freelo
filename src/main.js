import Vue from 'vue'
import axios from 'axios'
import Vuelidate from 'vuelidate'
import App from './App.vue'
import router from './router'
import ElementUI from 'element-ui';
import infiniteScroll from 'vue-infinite-scroll';
import 'element-ui/lib/theme-chalk/index.css';
import VueClipboard from 'vue-clipboard2'
import Notifications from 'vue-notification'
import store from './store'

var filter = function(text, length, clamp){
  clamp = clamp || '...';
  var node = document.createElement('div');
  node.innerHTML = text;
  var content = node.textContent;
  return content.length > length ? content.slice(0, length) + clamp : content;
};
Vue.filter('truncate', filter);

Vue.config.productionTip = false
Vue.use(ElementUI)
Vue.use(Vuelidate)
Vue.use(infiniteScroll)
Vue.use(VueClipboard)
Vue.use(Notifications)
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

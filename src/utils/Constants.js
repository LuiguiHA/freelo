// export const BASE_URL = 'http://localhost:9000/';
// export const BASE_URL = 'http://142.93.7.87:9000/';
// export const BASE_URL = 'https://lafokasocialssl.owson.com/api/';
export const BASE_URL = 'https://api.lafokasocialssl.owson.com/';

export const CONSTANTS = {
  API: {
    REGISTER: BASE_URL + 'user/register/',
    LOGIN: BASE_URL + 'user/login/',
    FORGOT_PASSWORD: BASE_URL + 'request-change-password/',
    MEMES: BASE_URL + 'posts/meme/',
    POST: alias => `${BASE_URL}p/${alias}/`,
    LIKE: alias => `${BASE_URL}p/${alias}/like/`,
    FOLLOW: username => `${BASE_URL}u/${username}/follow/`,
    UNFOLLOW: username => `${BASE_URL}u/${username}/unfollow/`,
    COMMENTS: alias => `${BASE_URL}p/${alias}/comments/`,
    REPORT: alias => `${BASE_URL}p/${alias}/report/`,
    REPORT_COMMENT: BASE_URL + 'p/comment/report/',
    REPLIES: function(alias, comment_alias) { return `${BASE_URL}p/${alias}/c/${comment_alias}/replies/` },
    LIKES_COMMENT: function(alias, comment_alias) { return `${BASE_URL}p/${alias}/c/${comment_alias}/like/` },
    DELETE_COMMENT: function(alias, comment_alias) { return `${BASE_URL}p/${alias}/c/${comment_alias}/delete/` },
    ME: BASE_URL + 'u/me/',
    PROFILE: username => `${BASE_URL}u/${username}/profile/`,
    PROFILE_POSTS: username => `${BASE_URL}u/${username}/posts/`,
    NOTIFICATIONS: BASE_URL + 'notification/list/',
    REPORT_POST_REASONS:  BASE_URL + 'report/post/reasons/',
    REPORT_POST_REASONS_COMMENT:  BASE_URL + 'report/comment/reasons/',
    LOGIN_FACEBOOK: BASE_URL + 'user/login/facebook/',
    CONNECT_FACEBOOK: BASE_URL + 'user/connect/facebook/',
    TRENDING: BASE_URL + 'posts/trending/',
    USERS: BASE_URL + 'users/search/',
    PUBLISH: BASE_URL + 'post/publish/',
    COMMENT: BASE_URL + 'p/comment/',
    DELETE_POST: alias => `${BASE_URL}p/${alias}/delete/`,
    EDIT_PROFILE: BASE_URL + 'u/me/update/',
    CHANGE_PASSWORD: BASE_URL + 'u/me/changepassword/',
    MARKED_NOTIFICATION: BASE_URL + 'notification/mark-read/'
  }
};

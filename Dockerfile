FROM gcr.io/cloud-marketplace/google/nginx:1.15
COPY nginx.conf /etc/nginx/conf.d/default.conf
COPY dist /usr/share/nginx/html
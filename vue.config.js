module.exports = {
    lintOnSave: true,
    // configureWebpack: {
    //     output: {
    //         path: __dirname + "/build"
    //     }
    // },
    devServer: {
        host: '0.0.0.0',
        hot: true,
        disableHostCheck: true,
    },
  }